Comment l'installer en production ?
=================================

_Ce document détaille seulement le cas d'une installation avec Apache 2 (wsgi) avec une base SQLite en utilisant un environement virtuel Python (virtualenv)_

N'oubliez pas de créer le fichier localsettings.py et de l'adapter à vos besoins.

# Créer un environnement virtuel (facultatif)

Si ce n'est pas déjà fait, installer les paquets (sous Debian) nécessaires :

    # apt-get install python-virtualenv virtualenvwrapper

Créer l'environnement :

    $ mkvirtualenv --python=/usr/bin/python3 firstjeudy

À partir de maintenant, l'on considère que vous travaillez dans votre environnement virtuel :

    $ workon firstjeudy

# Copier les fichiers dans le répertoire de destination par exemple /var/www/django/fj
Par exemple avec Git :

    $ git clone https://git.framasoft.org/microniko/firstjeudy.git

# Installer les paquets nécessaires

    $ pip install -r requirements.txt

# Créer le fichier firstjeudy/localsettings.py :

- SECRET_KEY : utilisez http://www.miniwebtool.com/django-secret-key-generator/ pour la générer.
- DATABASES : si la configuration par défaut (dans settings.py) ne vous convient pas.
- DEBUG = False
- TEMPLATE_DEBUG = False
- ALLOWED_HOSTS = ['fqdn']
- STATIC_ROOT="/var/...." : localisation des fichiers statiques
- ADMINS : liste des admins
- STATICFILES_DIRS = () : doit être vide en production
- MASTODON = {
        'url': '', ⇒ URL (avec http://) de l'instance
        'login': '', ⇒ Le nom d'utilisateur
        'pwd': "", ⇒ Le mot de passe
        }
# Créer la base de données

    $ python manage.py syncdb

Répondez aux questions posées.

# Charger les valeurs par défaut pour l'antispam (sinon, erreur au chargement d'une partie des pages)

    $ python manage.py loaddata antispam.json

# Lancer la commande suivante pour récupérer les fichiers statiques :

    $ python manage.py collectstatic

Assurez-vous que STATICFILES_DIRS est bien vide !

# Créer un hôte virtuel Apache

- Voir le fichier correspondant et l'adapter : vhost_apache.conf 
- Activer l'hôte virtuel et redémarrer Apache
 __Attention : "If you are using a version of Apache older than 2.4, replace Require all granted with Allow from all"__


# Rendez-vous dans /admin/ pour administrer votre Firstjeudy (utilisez le _superutilisateur_ créé à l'étape 4 pour vous connecter).

