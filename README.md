Qu'est-ce ?
===========
Permet de programmer des rendez-vous réguliers (ou non) avec inscription et localisation sur une carte.

Licence
=======
Ce script est sous licence GPL, voir fichier LICENCE

Historique
==========
* Firstjeudi a été écrit en PHP (3) initialement par : Fabien Seisen en 2001, Rodolphe Quiédeville en 2002.
* Puis adapté (PHP 4) en 2011 et 2012 par Nicolas Grandjean.
* La version actuelle en Django/Python reprend les mêmes principes initiaux en ajoutant une interface d'administration et la localisation de l'évènement en natif. Plus d'infos : http://wiki.rhizomes.org/index.php/Reprise_du_Firstjeudi_par_Rhizomes

Démonstration
=============
Sur www.firstjeudi.org, actuellement utilisé par le GULL breton Rhizomes.

Installation
============
Voir le fichier INSTALL.md

Mise à jour
===========
Voir le fichier UPGRADE.md

Traductions
===========
Pour générer le fichier .po :

   $ python manage.py makemessages --locale=



