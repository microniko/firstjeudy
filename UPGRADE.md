Mise à jour
===========


# Travailler dans l'environnement virtuel

    $ workon firstjeudy

# Récuperer la source

## Soit en clonant le dépôt

    $ git clone https://git.framasoft.org/microniko/firstjeudy.git

## Soit en synchronisant le dépôt local avec le dépôt distant

    $ git pull

# Mettre à jour les paquets de l'environnement virtuel

    $ pip install -r requirements.txt


# Lancer le script de mirgrations des données

    $ python manage.py migrate

# Lancer la commande suivante pour récupérer les fichiers statiques :

    $ python manage.py collectstatic

Cela aura pour effet de supprimer le contenu initial de STATICFILES_DIR.

# Redémarrer le serveur HTTP (Apache)

# Vous pouvez commencer à utiliser la nouvelle version :)
