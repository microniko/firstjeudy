# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Questions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.CharField(max_length=100, verbose_name='Question')),
                ('reponse', models.CharField(max_length=100, verbose_name='R\xe9ponse')),
            ],
            options={
                'verbose_name': "Question de l'antispam",
                'verbose_name_plural': "Questions de l'antispam",
            },
            bases=(models.Model,),
        ),
    ]
