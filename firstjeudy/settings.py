# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Django settings for firstjeudy project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ''

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['domaine.org']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'fj',
#    'redactor',
#    'tinymce',
    'leaflet',
    'djgeojson',
#    'django.contrib.gis',
    'ponctu',
    'antispam',
    'ckeditor',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.i18n",
)

ROOT_URLCONF = 'firstjeudy.urls'

WSGI_APPLICATION = 'firstjeudy.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.firstjeudy.sqlite3'),
    }
}
#DATABASES = {
#    'default': {
#         'ENGINE': 'django.contrib.gis.db.backends.spatialite',
#         'NAME': os.path.join(BASE_DIR, 'db.geodjango'),
#     }
#}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/
from django.utils.translation import gettext_lazy as _

# Nécessaire ?
#SITE_ID = 1

LANGUAGE_CODE = 'fr'

TIME_ZONE = 'Europe/Paris'

USE_I18N = True

USE_L10N = True

LANGUAGES = (
    ('fr', _('Français')),
    ('en', _('Anglais')),
)

DEFAULT_LANGUAGE = 1

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'


STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

# Activer au déploiement (python manage.py collectstatic afin de récupérer les fichiers statics de l'admin)
#STATIC_ROOT = '/var/www/firstjeudi.org/firstjeudy/static/'
STATIC_ROOT=''


# Administrateurs
# La première valeur du tuple sera utilisé dans le champ « From: » des courriels envoyé par le système
ADMINS = (
    ("G.", "xxx@xxx.fr"),
)

# Gabarits
TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)


#Ajoute un Slash à la fin de chaque URL
APPEND_SLASH = True

# Courrels
#EMAIL_HOST = 'smtp.fai.fr'

# Redactor
#REDACTOR_OPTIONS = {'lang': 'fr',
#    'imageUpload': False,
#    'linkFileUpload': False,
#    'fileUpload': False,
#    'focus': False,
#}
#REDACTOR_UPLOAD = 'uploads/'

#TINYMCE_JS_URL = STATIC_URL + 'js/tinymce/tinymce.min.js'
#TINYMCE_DEFAULT_CONFIG = {
#        'plugins': "table,spellchecker,paste,searchreplace",
#        'theme': "advanced",
#        'cleanup_on_startup': True,
#        'custom_undo_redo_levels': 10,
#}
#TINYMCE_SPELLCHECKER = True
#TINYMCE_COMPRESSOR = True

# Config Leaflet
LEAFLET_CONFIG = {
    'DEFAULT_CENTER': (47.4, 1.8),
    'DEFAULT_ZOOM': 6,
    'MIN_ZOOM': 3,
    'MAX_ZOOM': 18,
    #'ATTRIBUTION_PREFIX': 'Powered by django-leaflet'
}

#MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
#MEDIA_URL =  "/media/"
# Endroit des images
MEDIA_ROOT = BASE_DIR + "/fichiers"
#MEDIA_ROOT = os.path.dirname(os.path.dirname(__file__))+"/images"
MEDIA_URL = "/fichiers/"

CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_JQUERY_URL = STATIC_URL + 'js/jquery2.min.js'

# Nécessaire pour le rappel du login
LOGIN_URL = '/admin/'

#CKEDITOR_CONFIGS = {
#        'default': {
#                    "toolbar": "",
#                },
#}

# Configuration locale
# Créez votre propre fichier local_settings.py afin de définir votre configuration personnelle
try:
    from local_settings import *
except ImportError:
    pass
