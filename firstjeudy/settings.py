# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Django settings for firstjeudy project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ''

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['domaine.org']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'fj',
    'leaflet',
    'djgeojson',
#    'django.contrib.gis',
    'ponctu',
    'antispam',
    'lieux',
    'tinymce',
    #'django_extensions',
)

MIDDLEWARE = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
                os.path.join(BASE_DIR, 'templates'),
            ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
            ],
            'debug': True,
        },
    },
]

ROOT_URLCONF = 'firstjeudy.urls'

WSGI_APPLICATION = 'firstjeudy.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.firstjeudy.sqlite3'),
    }
}
#DATABASES = {
#    'default': {
#         'ENGINE': 'django.contrib.gis.db.backends.spatialite',
#         'NAME': os.path.join(BASE_DIR, 'db.geodjango'),
#     }
#}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/
from django.utils.translation import gettext_lazy as _

# Nécessaire ?
#SITE_ID = 1

LANGUAGE_CODE = 'fr'

TIME_ZONE = 'Europe/Paris'

USE_I18N = True

USE_L10N = True

LANGUAGES = (
    ('fr', _('Français')),
    ('en', _('Anglais')),
)

DEFAULT_LANGUAGE = 1

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'


STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

# Activer au déploiement (python manage.py collectstatic afin de récupérer les fichiers statics de l'admin)
#STATIC_ROOT = '/var/www/firstjeudi.org/firstjeudy/static/'
STATIC_ROOT=''


# Administrateurs
# La première valeur du tuple sera utilisé dans le champ « From: » des courriels envoyé par le système
ADMINS = (
    ("G.", "xxx@xxx.fr"),
)

#Ajoute un Slash à la fin de chaque URL
APPEND_SLASH = True

# Courrels
#EMAIL_HOST = 'smtp.fai.fr'

# Config Leaflet
LEAFLET_CONFIG = {
    'DEFAULT_CENTER': (47.4, 1.8),
    'DEFAULT_ZOOM': 6,
    'MIN_ZOOM': 3,
    'MAX_ZOOM': 18,
    #'ATTRIBUTION_PREFIX': 'Powered by django-leaflet'
}

#MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
#MEDIA_URL =  "/media/"
# Endroit des images
MEDIA_ROOT = BASE_DIR + "/fichiers"
#MEDIA_ROOT = os.path.dirname(os.path.dirname(__file__))+"/images"
MEDIA_URL = "/fichiers/"

# Nécessaire pour le rappel du login
LOGIN_URL = '/admin/'

# Localisation de l'administration
# Personnaliser cette valeur est fortement conseillée.
ADMIN_URL = 'admin42'

TINYMCE_DEFAULT_CONFIG = {
            'plugins': "table,paste,searchreplace",
            'cleanup_on_startup': True,
            'min_height': 200,
            'width': 700,
            }

# Configuration locale
# Créez votre propre fichier local_settings.py afin de définir votre configuration personnelle
try:
    from firstjeudy.local_settings import *
except ImportError as e:
    pass
