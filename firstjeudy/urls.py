# -*- coding: utf-8 -*-
# 
# Copyright (C) 2015  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import include, url
from fj.views import liste_villes

from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static

from django.contrib.auth import views as auth_views

from django.contrib import admin
from django.contrib.gis import admin as geoadmin

# Pour le robots.txt
from django.http import HttpResponse

admin.autodiscover()


urlpatterns = [
    # Examples:
    # url(r'^$', 'firstjeudy.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # TinyMCE
    url(r'^tinymce/', include('tinymce.urls')),
    # Administration
    url(r'^' + settings.ADMIN_URL +'/', geoadmin.site.urls),

    # Rappel de mot de passe
    url('^' + settings.ADMIN_URL , include('django.contrib.auth.urls')),
    url(
    r'^' + settings.ADMIN_URL +'/password_reset/',
    auth_views.PasswordResetView.as_view(),
    name='admin_password_reset',
    ),
    url(
        r'^' + settings.ADMIN_URL +'/password_reset/done/',
        auth_views.PasswordResetDoneView.as_view(),
        name='password_reset_done',
    ),
    url(
        r'^' + settings.ADMIN_URL +'/reset/<uidb64>/<token>/',
        auth_views.PasswordResetConfirmView.as_view(),
        name='password_reset_confirm',
    ),
    url(
        r'^' + settings.ADMIN_URL + '/reset/done/',
        auth_views.PasswordResetCompleteView.as_view(),
        name='password_reset_complete',
    ),

    # Page d'index  : la liste des villes
    url(r'^$', liste_villes, name='index'),

    # Robots.txt
    url(r'^robots.txt', lambda x: HttpResponse("User-Agent: *\nDisallow:", content_type="text/plain"), name="robots_file"),

    url('^r/', include('ponctu.urls')),

    url('^lieu/', include('lieux.urls')),

    # Flux iCal
    #url(r'^flux$', Flux()),

    url('^', include('fj.urls')),
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

