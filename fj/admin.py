# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.contrib import admin
from leaflet.admin import LeafletGeoAdmin
from fj.models import Ville, Inscrits
from django import forms

class VilleAdmin(admin.ModelAdmin):
    fields = ('code', 'nom', 'message', 'description', 'pouet', 'email',
            'lieu', 'tag',
            ('numday', 'numweek', 'debut', 'fin', 'limite'),
            'autorise_inscription','actif'
            )
    view_on_site = True
    save_on_top = True
    save_as = True
    list_display =  ('code',  'nom')

admin.site.register(Ville,VilleAdmin)


# Inscriptions
class InscritsAdmin (admin.ModelAdmin):
    list_display = ('prenom', 'nom', 'nb', 'asso', 'num', 'ville', 'ip')
    list_filter = ('asso', 'ville', 'num',)

admin.site.register(Inscrits, InscritsAdmin)

