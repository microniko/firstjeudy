# -*- coding: utf-8 -*-
#
#  forms.py
#
#  Copyright 2014 Nicolas Grandjean <nicolas@microniko.net>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
from django import forms
from models import Inscrits
from django.utils.translation import gettext_lazy as _
import random

from antispam.models import Questions

class InscritsForm(forms.ModelForm):
    """ Formulaire d'inscription """
    nombre    = Questions.objects.all().count()     # Nombre de questions dans la base
    aleatoire = random.randint(1,nombre)            # Un id au hasard
    question  = Questions.objects.get(pk=aleatoire) # On en choisie une au hasard
    verification = forms.CharField(required=True,max_length=20, label=question.question)  # Le champ de formulaire avec le libellé de la question
    ident = forms.CharField(initial=str(aleatoire), widget=forms.HiddenInput()) # On transmet l'id de la question 
    class Meta:
        model = Inscrits
        fields = ('nom', 'prenom', 'asso', 'courriel', 'nb')

    def clean(self):
        cleaned_data = super(InscritsForm, self).clean()
        verif = cleaned_data.get('verification')
        ident = cleaned_data.get('ident')
        reponse = Questions.objects.get(pk=int(ident)).reponse.encode('utf-8')
        if verif:
            verif = cleaned_data.get('verification').encode('utf-8')
            print "verif → " + str(verif)
            print "ident → "+ str(ident)
            print "reponse → " + str(reponse)
            if reponse.upper() != verif.upper():
                raise forms.ValidationError(_("Désolé !"))
        else:
            raise forms.ValidationError(_("Répondez à la question"))

