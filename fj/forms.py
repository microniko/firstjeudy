# -*- coding: utf-8 -*-
#
#  forms.py
#
#  Copyright 2014 Nicolas Grandjean <nicolas@microniko.net>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
from django import forms
from fj.models import Inscrits
from django.utils.translation import gettext_lazy as _
from random import randint
from django.db.models import Min, Max

from antispam.models import Questions

class InscritsForm(forms.ModelForm):
    """ Formulaire d'inscription """
    boundaries = Questions.objects.aggregate(Min('id'), Max('id'))# Les extrémités
    question = None
    essais = 0
    while question is None:
        try:
            question  = Questions.objects.get(pk=randint(boundaries['id__min'], boundaries['id__max'])) # On en choisie une au hasard
        except Questions.DoesNotExist:
            essais += 1
        else:
            break
    print(essais)
    verification = forms.CharField(required=True,max_length=20, label=question.question)  # Le champ de formulaire avec le libellé de la question
    ident = forms.CharField(initial=str(question.pk), widget=forms.HiddenInput()) # On transmet l'id de la question
    class Meta:
        model = Inscrits
        fields = ('nom', 'prenom', 'asso', 'courriel', 'nb')

    def clean(self):
        cleaned_data = super(InscritsForm, self).clean()
        verif = cleaned_data.get('verification')
        ident = cleaned_data.get('ident')
        reponse = Questions.objects.get(pk=int(ident)).reponse.encode('utf-8')
        if verif:
            verif = cleaned_data.get('verification').encode('utf-8')
    #        print "verif → " + str(verif)
     #       print "ident → "+ str(ident)
      #      print "reponse → " + str(reponse)
            if reponse.upper() != verif.upper():
                raise forms.ValidationError(_("Désolé !"))
        else:
            raise forms.ValidationError(_("Répondez à la question"))

class AgendaForm(forms.Form):
    """ Formulaire de soumission à l'Agenda du Libre """
    email = forms.EmailField(label="Adresse du soumetteur", max_length=100)
    toot = forms.BooleanField(label=_("En profiter pour envoyer un pouet sur Mastodon"),required=False)
