# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2017-03-25 21:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fj', '0002_separation_lieux_table_specifique'),
    ]

    operations = [
        migrations.AddField(
            model_name='ville',
            name='tag',
            field=models.CharField(max_length=364, null=True, verbose_name="Tags (utile pour la soumission à l'Agenda du Libre <em>Séparés par des espaces, ils ne peuvent contenir que des lettres minuscules, des chiffres et des tirets. Ajoutez le nom de la ou des organisations de l'événement, mais pas de la ville ou de la région.</em>"),
        ),
        migrations.AlterField(
            model_name='ville',
            name='email',
            field=models.TextField(default='Bonjour #PRENOM #NOM.\n\nVotre inscription a bien été prise en compte (pour #NB) :\n- Ville : #VILLE\n- Date : #DATE\n- Cette rencontre aura lieu à #LIEU\n- Adresse IP : #IP\n\nPour annuler votre inscription, vous pouvez cliquer sur le lien suivant : \n#URL_DEL', help_text="Les mots-clés suivants peuvent être utilisés :<br>#NOM : Nom de l'inscrit<br>#PRENOM : prénom de l'inscrit<br>#ASSO : Association<br>#VILLE : nom complet de la ville<br>#LIEU : Nom du lieu de la rencontre<br>#DATE : date de la rencontre<br>#NB : nombre d'inscrit<br>#IP : adresse IP de l'inscrit<br>#URL_DEL : adresse de désinscription", verbose_name="Message sur le courriel de confirmation d'inscription"),
        ),
    ]
