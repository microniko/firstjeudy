# Generated by Django 2.0.7 on 2018-09-07 21:33

from django.db import migrations, models
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('fj', '0005_auto_20180415_2317'),
    ]

    operations = [
        migrations.AddField(
            model_name='ville',
            name='pouet',
            field=models.TextField(help_text="Les mots-clés peuvent-être utilisés :<br>#LIEU_NOM : le nom du lieu<br>#LIEU_ADRESSE : l'adresse du lieu<br>#HEURE_DEBUT : l'heure de début<br>#HEURE_FIN : l'heure de fin", max_length=500, null=True, verbose_name='Message à envoyer sur Mastodon'),
        ),
        migrations.AlterField(
            model_name='ville',
            name='message',
            field=tinymce.models.HTMLField(help_text="Apparaît sur la page de la rencontre. Les mots-clés suivants peuvent êtres utilisés :<br>#LIEU_NOM : le nom du lieu<br>#LIEU_URL : l'URL vers la page du lieu<br>#LIEU_ADRESSE : adresse du lieu<br>#HEURE_DEBUT : début de l'évènement<br>#HEURE_FIN : Fin de l'évènement", verbose_name='Message long'),
        ),
    ]
