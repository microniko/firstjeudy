# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.test import TestCase
from fj.models import *
import uuid

def lipsum(long=10):
      """ Une bête fonction pour retourner du Lorem Ipsum facilement dans une longeur prédéfinie"""
      lip=u"Lorem ipsum dolor sit amet, consecteteur adipiscing. Duis. Mi, placerat. Integer at iaculis quam etiam volutpat ante, senectus maecenas primis semper fames ligula ad nisl mattis sit. Eros ve nulla praesent cras dis eni. Diam duis blandit ac etiam litora. Enim. Varius. Mus class euismod sociosqu velit. Iaculis libero. Pharetra, euismod metus orci. Amet eni vitae vel diam arcu massa ve sollicitudin rhoncus sem."
      return lip[0:long]

class TestVille(TestCase):
    """ Tests unitaires pour tester les villes """
    def test_creation_ville(self):
        """ Test de création d'une ville """
        ville = Ville(code = u"CODE" ,
                      message = u"<p>"+ lipsum(30) + u"</p>" +
                                u"<p>"+ lipsum(40) + u"</p>" +
                                u"<p>"+ lipsum(17) + u"</p>" +
                                u"<p>"+ lipsum(22) + u"</p>",
                      description = u"<p>"+ lipsum(60) + u"</p>" +
                                    u"<p>"+ lipsum(28) + u"</p>" +
                                    u"<p>"+ lipsum(70) + u"</p>" +
                                    u"<p>"+ lipsum(17) + u"</p>" +
                                    u"<p>"+ lipsum(49) + u"</p>" +
                                    u"<p>"+ lipsum(22) + u"</p>",
                      champGeo = '{"type":"Point","coordinates":[22.390136718749996,37.04816272916948]}',
                      zoom = 8.0,
                      numday = 5,
                      numweek = 2)
        ville.save()
        v = Ville.objects.get(code = u"CODE")
        self.assertEqual(v.zoom, 8.0)


#class TestInscrits(TestCase):
    """ Tests unitaires pour tester les inscriptions """
    def test_creation_inscrits(self):
        """ Test d'inscription """
        self.test_creation_ville()
        v = Ville.objects.get(code= u"CODE")
        inscription = Inscrits(unique = str(uuid.uuid4()),
                               nom = lipsum(40),
                               prenom = lipsum(40),
                               asso = lipsum(150),
                               verif = u"quatre",
                               courriel = u"toto",
                               nb = 5,
                               num = "20140112",
                               ville = v.id,
                               ip = "192.168.1.1")
        inscription.save()
        ins = Inscription.objects.get(courriel=u"toto")
        self.assertEqual(ip, "192.168.1.1")
