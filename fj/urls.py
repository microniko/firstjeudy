#-*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from fj.views import InscritsDelete
#from ponctu.views import InscritsDeletePonctu

from django.conf import settings
#from django.conf.urls.static import static

#from django.contrib import admin
#from django.contrib.gis import admin as geoadmin
#admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'firstjeudy.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # Page d'index  : la liste des villes
#    url(r'^$', ListeVilles.as_view(), name='index'),
#    url(r'^$', 'fj.views.liste_villes', name='index'),

    # Page de désinscription pour les régulières
    url(r'^del/(?P<unique>.+)$', InscritsDelete.as_view(), name='inscrit_delete'),

    # Page d'une ville : inscription
    url(r'^(?P<code>.+)$', 'fj.views.inscription', name="voir_ville"),

    #url(r'^(?P<code>.+)/stats$', 'fj.views.stats'),

)
