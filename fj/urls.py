#-*- coding: utf-8 -*-

from django.conf.urls import include, url
from django.urls import path
from fj.views import InscritsDelete, inscription, adl, ical
#from ponctu.views import InscritsDeletePonctu

from django.contrib.admin.views.decorators import staff_member_required
from django.views.generic.base import TemplateView

from django.conf import settings
#from django.conf.urls.static import static

#from django.contrib import admin
#from django.contrib.gis import admin as geoadmin
#admin.autodiscover()

app_name = 'fj'
urlpatterns = [
    # Examples:
    # url(r'^$', 'firstjeudy.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # Page d'index  : la liste des villes
#    url(r'^$', ListeVilles.as_view(), name='index'),
#    url(r'^$', 'fj.views.liste_villes', name='index'),

    # Page de désinscription pour les régulières
    path('del/<str:unique>', InscritsDelete.as_view(), name='inscrit_delete'),

    # Fichier ICAL
    path('<str:code>.ics',
        ical,
        name='ical'
        ),
    #url(r'^(?P<code>.+)/stats$', 'fj.views.stats'),
    # Page pour soumettre à l'Agenda du Libre
    path('agenda/<str:code>',
        adl,
        name='adl'
        ),


    # Page d'une ville : inscription
    path('<str:code>', inscription, name="voir_ville"),

]
