# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Bricoles pour l'exécution du script de soumission
import os
import tempfile
import subprocess

from django.shortcuts import render
from django.http import Http404
from django.shortcuts import render, get_object_or_404

import uuid
from django import http
from django.views.generic import ListView, DeleteView

from django.urls import reverse_lazy

# Import de bidules pour le calcul de date
from dateutil.rrule import *
from dateutil.parser import *
from datetime import *

# dates en français
import locale, time
locale.setlocale(locale.LC_TIME, "fr_FR.utf8")

# Filtrage par date
import datetime

# Export iCal
#from django_ical.views import ICalFeed

# Envoi de courriels
from django.core.mail import send_mail

# Mastodon
from mastodon import Mastodon

from fj.models import Ville, Inscrits
from fj.forms import InscritsForm, AgendaForm
from django.utils.translation import gettext_lazy as _

from ponctu.models import Rencontre

from firstjeudy import  settings


def liste_villes(request):
    # La liste des villes
    liste_villes = Ville.objects.filter(actif=True)

    # La liste des rencontres ponctuelles
    liste_rencontres = Rencontre.objects.filter(date__gte = datetime.datetime.now())

    # Le gabartit
    return render(request, 'index.html', locals())

# ------- Page d'inscription     ----------------
def inscription(request, code):
    """Afficher la page d'inscription
    - D'abord, sa description
    - Ensuite le formulaire d'inscription
    - Puis le menu
    - Et enfin, la liste des inscrits
    """

    # Pour la description de la ville :
    ville = get_object_or_404(Ville, code=code, actif=True)

    ville.message = ville.message.replace(u"#LIEU_NOM",str(ville.lieu.nom))
    ville.message = ville.message.replace(u"#LIEU_ADRESSE",str(ville.lieu.adresse))
    ville.message = ville.message.replace(u"#LIEU_URL",'/lieu/' + str(ville.lieu.pk))
    if ville.debut != None:
        ville.message = ville.message.replace(u"#HEURE_DEBUT",ville.debut.strftime("%Hh%M"))
    if ville.fin != None:
        ville.message = ville.message.replace(u"#HEURE_FIN",ville.fin.strftime("%Hh%M"))

    jr = (
        (0,SU(ville.numweek)),
        (1,MO(ville.numweek)),
        (2,TU(ville.numweek)),
        (3,WE(ville.numweek)),
        (4,TH(ville.numweek)),
        (5,FR(ville.numweek)),
        (6,SA(ville.numweek)),
        )

    jour = list(rrule(MONTHLY, count=1, byweekday=jr[ville.numday][1]))

    TropTard=False

    for foo in jour:
        if ville.debut:
            # La date/heure réelle de l'évènement
            reel = datetime.datetime(foo.year, foo.month, foo.day, ville.debut.hour, ville.debut.minute)
            if ville.limite:
                if (datetime.datetime.now() > reel - timedelta(hours = ville.limite)):
                    # Si maintenant est supérieur à la date réel de limite d'inscription
                    TropTard= True
        #jour = foo.strftime('%A %d %B %Y').capitalize()  # Avec zéro initial dans le jour du mois
        jour = foo.strftime('%A %e %B %Y').capitalize()   # sans                                   (seulement sous *nix)
        court = foo.strftime("%Y%m%d")
        humain = str(foo.strftime('%A %e %B %Y'))  # idem

    if TropTard == False or (TropTard == True and ville.autorise_inscription == True):
        # Le formulaire d'inscription
        if request.method== 'POST':
            form = InscritsForm(request.POST)
            if form.is_valid():
                unique=str(uuid.uuid4())
                adresse_ip =request.META["REMOTE_ADDR"]
                url_fj =request.META["SERVER_NAME"]
                # Est-ce une IPv6 ?
                longueur = len(adresse_ip)
                i=0
                v6=""
                while i < longueur:
                    if adresse_ip[i] == ":":
                        v6= u"IPv6 :-)"
                    i = i + 1

                # Sauvegarde en base
                Inscrits(
                    nom=form.cleaned_data['nom'],
                    prenom=form.cleaned_data['prenom'],
                    asso=form.cleaned_data['asso'],
                    courriel=form.cleaned_data['courriel'],
                    nb=form.cleaned_data['nb'],
                    unique=unique,
                    num=court,
                    ville_id=ville.id,
                    ip=adresse_ip
                    ).save()
                # Envoi d'un courriel
                # Préparation des variables
                corps = ville.email.replace(u"#PRENOM",form.cleaned_data['prenom'])
                corps = corps.replace(u"#NOM",form.cleaned_data['nom'])
                corps = corps.replace(u"#NB",str(form.cleaned_data['nb']))
                corps = corps.replace(u"#ASSO",form.cleaned_data['asso'])
                corps = corps.replace(u"#VILLE",ville.nom)
                corps = corps.replace(u"#LIEU",ville.lieu.nom)
                corps = corps.replace(u"#URL_LIEU",'http://' + url_fj + '/lieu/'
                        + str(ville.lieu.pk))
                corps = corps.replace(u"#DATE",humain)
                corps = corps.replace(u"#HEURE_DEBUT",str(ville.debut))
                corps = corps.replace(u"#HEURE_FIN",str(ville.fin))
                corps = corps.replace(u"#IP",adresse_ip + " " + v6)
                corps = corps.replace(u"#URL_DEL",'http://' + url_fj + '/del/'  +unique)
                corps = corps.replace(u"#URL_ICAL",'http://' + url_fj + '/' + ville.code + '.ics')
                send_mail(
                    ville.nom +' du ' + humain,
                    corps,
                    settings.ADMINS[0][0] + " <" + settings.ADMINS[0][1] + ">",
                    [form.cleaned_data['courriel']],
                    fail_silently=False
                    )
                ok = True
        else:
            form=InscritsForm()

    # La liste des villes
    liste_villes = Ville.objects.filter(actif=True)

    # La liste des rencontres ponctuelles
    liste_rencontres = Rencontre.objects.filter(date__gte = datetime.datetime.now())
    # La liste des inscrits
    liste_inscrits = Inscrits.objects.filter(ville_id=ville.id, num=court)
    total = 0
    for insc in liste_inscrits:
        total = total + insc.nb


    # Le gabartit
    return render(request, 'ville.html', locals())

from django.contrib.admin.views.decorators import staff_member_required

@staff_member_required
def adl(request, code):
    """ Page de soumission directe pour l'Agenda Du Libre
        et à Mastodon
    """

    # Pour la description de la ville :
    ville = get_object_or_404(Ville, code=code, actif=True)

    jr = (
        (0,SU(ville.numweek)),
        (1,MO(ville.numweek)),
        (2,TU(ville.numweek)),
        (3,WE(ville.numweek)),
        (4,TH(ville.numweek)),
        (5,FR(ville.numweek)),
        (6,SA(ville.numweek)),
        )

    jour = list(rrule(MONTHLY, count=1, byweekday=jr[ville.numday][1]))

    for foo in jour:
        if ville.debut:
            date_adl = foo.strftime("%Y-%m-%d")
        jour = foo.strftime('%A %e %B %Y').capitalize()   # sans                                   (seulement sous *nix)
        contact_adl = settings.ADMINS[0][1]
        url = "http://" + request.META["SERVER_NAME"] + "/" + ville.code
        ville.message = ville.message.replace(u"#LIEU_NOM",str(ville.lieu.nom))
        ville.message = ville.message.replace(u"#LIEU_ADRESSE",str(ville.lieu.adresse))
        ville.message = ville.message.replace(u"#LIEU_URL", "http://" + request.META["SERVER_NAME"] +'/lieu/' + str(ville.lieu.pk))
        if ville.debut != None:
            ville.message = ville.message.replace(u"#HEURE_DEBUT",ville.debut.strftime("%Hh%M"))
        if ville.fin != None:
            ville.message = ville.message.replace(u"#HEURE_FIN",ville.fin.strftime("%Hh%M"))
        if ville.pouet !=None:
            ville.pouet = ville.pouet.replace(u"#URL",url)
            ville.pouet = ville.pouet.replace(u"#DATE",jour)
            ville.pouet = ville.pouet.replace(u"#LIEU_NOM",str(ville.lieu.nom))
            ville.pouet = ville.pouet.replace(u"#LIEU_ADRESSE",str(ville.lieu.adresse_adl))
            ville.pouet = ville.pouet.replace(u"#LIEU_VILLE",str(ville.lieu.ville_adl))
            ville.pouet = ville.pouet.replace(u"#LIEU_REGION",str(ville.lieu.region_adl))
            if ville.debut != None:
                ville.pouet = ville.pouet.replace(u"#HEURE_DEBUT",ville.debut.strftime("%Hh%M"))
            if ville.fin != None:
                ville.pouet = ville.pouet.replace(u"#HEURE_FIN",ville.fin.strftime("%Hh%M"))


    if request.method== 'POST':
        form = AgendaForm(request.POST)
        if form.is_valid():
            BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
            script = BASE_DIR + '/adl-submit.py'
            with tempfile.TemporaryDirectory() as tempdir:
                # Écrire le fichier XML
                fichier = tempdir + "/" + str(time.time()) + ".xml"
                f = open(fichier, "w", encoding="utf8")
                f.write(request.POST['xml'])
                f.close()
                cmd = subprocess.Popen([script, '--submitter', request.POST['email'], '--file', fichier], stdout=subprocess.PIPE)
                resultat = cmd.communicate()[0]
            if 'toot' in request.POST and ville.pouet!=None:
                with tempfile.TemporaryDirectory() as tempdir:
                    temporaire = tempdir + str(time.time())
                    Mastodon.create_app(
                         'pytooterapp',
                         api_base_url = settings.MASTODON['url'],
                         to_file = temporaire + 'pytooter_clientcred.secret'
                    )
                    mastodon = Mastodon(
                        client_id = temporaire + 'pytooter_clientcred.secret',
                        api_base_url = settings.MASTODON['url'],
                    )
                    mastodon.log_in(
                        settings.MASTODON['login'],
                        settings.MASTODON['pwd'],
                        to_file = temporaire + 'pytooter_usercred.secret'
                    )

                    mastodon = Mastodon(
                        access_token = temporaire + 'pytooter_usercred.secret',
                        api_base_url = settings.MASTODON['url'],
                    )
                    pouet =  ville.pouet
                    mastodon.toot(pouet[:500])
                    resultatMastodon = "Pouet envoyé !"

    else:
        form=AgendaForm()

    # La liste des villes
    liste_villes = Ville.objects.filter(actif=True)
    # La liste des rencontres ponctuelles
    liste_rencontres = Rencontre.objects.filter(date__gte = datetime.datetime.now())

    # Le gabartit
    return render(request, 'agenda.html', locals())


# ------- Page de suppression     ----------------
class InscritsDelete(DeleteView):
    model = Inscrits
    context_object_name = "supprime_inscrit"
    template_name = 'del.html'
    success_url = reverse_lazy('index')
    supprime = True
    def get_object(self, queryset=None):
        unique = self.kwargs.get('unique', None)
        return get_object_or_404(Inscrits, unique=unique)

# ----------- Export iCal -------------------------

def ical(request, code):
    """ Génération du fichier Ical (code.ics)
    """

    # Pour la description de la ville :
    ville = get_object_or_404(Ville, code=code, actif=True)

    ville.message = ville.message.replace(u"#LIEU_NOM",str(ville.lieu.nom))
    ville.message = ville.message.replace(u"#LIEU_ADRESSE",str(ville.lieu.adresse))
    ville.message = ville.message.replace(u"#LIEU_URL",'/lieu/' + str(ville.lieu.pk))
    if ville.debut != None:
        ville.message = ville.message.replace(u"#HEURE_DEBUT",ville.debut.strftime("%Hh%M"))
    if ville.fin != None:
        ville.message = ville.message.replace(u"#HEURE_FIN",ville.fin.strftime("%Hh%M"))

    jr = (
        (0,SU(ville.numweek)),
        (1,MO(ville.numweek)),
        (2,TU(ville.numweek)),
        (3,WE(ville.numweek)),
        (4,TH(ville.numweek)),
        (5,FR(ville.numweek)),
        (6,SA(ville.numweek)),
        )

    jour = list(rrule(MONTHLY, count=1, byweekday=jr[ville.numday][1]))

    for foo in jour:
        if ville.debut:
            jour = foo.strftime("%Y%m%d")
            veille = foo-datetime.timedelta(1) 
            veille.strftime("%Y%m%d")
        url = "http://" + request.META["SERVER_NAME"] + "/" + ville.code
    return render(request, "ical.ics", locals(), content_type='text/calendar')
