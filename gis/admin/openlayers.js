{% extends "gis/admin/openlayers.js" %}

{% block extra_layers %}
    topo_layer = new OpenLayers.Layer.OSM( "Simple OSM Map" );
    {{ module }}.map.addLayer(topo_layer);
{% endblock extra_layers %}
