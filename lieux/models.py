# Copyright (C) 2016  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models

from tinymce import models as tinymce_models
# Create your models here.

from djgeojson.fields import PointField
from django.utils.translation import gettext_lazy as _
from django.core.validators import MaxValueValidator, MinValueValidator, RegexValidator

class Lieu(models.Model):
    """ Les lieux des évènements """
    nom = models.CharField(verbose_name=_("Nom"), max_length=30, unique=True, null=False, help_text=_("Ce champ apparaît sur la page d'accueil (récapitulatif) et dans le courriel envoyé lors d'une inscription."))
    adresse = tinymce_models.HTMLField(
        verbose_name=_("Adresse"), help_text=_("Indiquer les informations complètes du lieu (nom, adresse) du lieu. Ce champ apparaît sur le site"),
        )
    adresse_adl = models.CharField(
            verbose_name=_("Adresse (2)"), help_text=_("Indiquer l'adresse (utile pour la soumission à l'Agenda du Libre)"),
            null=True,
            max_length=352,
            )
    ville_adl = models.CharField(
            verbose_name=_("Ville"), help_text=_("Indiquer la ville (utile pour la soumission à l'Agenda du Libre"),
            max_length=70,
            null=True,
            )
    region_adl = models.CharField(
            verbose_name=_("Région"), help_text=_("Indiquer la région (utile pour la soumission à l'Agenda du Libre"),
            max_length=70,
            )
    champGeo = PointField(verbose_name=_("Lieu"), help_text=_("Cliquer sur le marqueur puis cliquer sur la carte pour indiquer le lieu de l'évènement."))
    zoom = models.FloatField(verbose_name=_("Zoom de la carte"), null=False, default=13, validators=[MinValueValidator(0), MaxValueValidator(19)])
    def get_absolute_url(self):
        return "/lieu/{0}".format(self.pk)
    def __str__ (self):
        return self.nom
    class Meta:
        verbose_name="Lieu"
        verbose_name_plural="Lieux"
