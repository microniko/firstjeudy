#-*- coding: utf-8 -*-

from django.conf.urls import include, url
from django.urls import path
from lieux.views import LieuDetailView
#from ponctu.views import InscritsDeletePonctu

#from django.contrib.admin.views.decorators import staff_member_required
from django.views.generic.base import TemplateView

from django.conf import settings
#from django.conf.urls.static import static

#from django.contrib import admin
#from django.contrib.gis import admin as geoadmin
#admin.autodiscover()

app_name = 'lieux'
urlpatterns = [
    # Examples:
    # url(r'^$', 'firstjeudy.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # Page d'index  : la liste des villes
#    url(r'^$', ListeVilles.as_view(), name='index'),
#    url(r'^$', 'fj.views.liste_villes', name='index'),

    # Page d'une ville : inscription
    path('<str:pk>', LieuDetailView.as_view(), name="voir_lieu"),

]
