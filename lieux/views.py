from django.shortcuts import render

# Create your views here.


from django.views.generic.detail import DetailView
from lieux.models import Lieu

class LieuDetailView(DetailView):
        model = Lieu
        template_name = "lieu.html"
