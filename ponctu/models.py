# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.db import models

# Create your models here.

# Zones de texte avec du Wysiwyg
from ckeditor.fields import RichTextField

# Create your models here.
from djgeojson.fields import PointField
from django.core.exceptions import ValidationError

from django.core.validators import MaxValueValidator, MinValueValidator

from django.utils.translation import gettext_lazy as _

# Description de la base de données
#

def pas_plus_de_cinq(value):
   if value > 5:
      raise ValidationError(_("C'est un peu trop, %s, non ?!") % value)

# --- Rencontre

class Rencontre(models.Model):
    code = models.CharField(verbose_name=_("Raccourci (utilisé dans les URL)"), max_length =10, unique=True, null=False)
    nom = models.CharField(verbose_name=_("Nom complet"), max_length=30, unique=True, null=False)
    #message = tinymce_models.HTMLField(
    message = RichTextField(
        verbose_name=_("Message long (apparaît sur la page de la Recontre)"),
        )
    #description = tinymce_models.HTMLField(
    description = RichTextField(
        verbose_name=_("Message plus court (apparaît sur le résumé)"),
        )
    email = models.TextField(verbose_name=_("Message sur le courriel de confirmation d'inscription"),default=_("Bonjour #PRENOM #NOM.\n\nVotre inscription (pour #NB) a bien été prise en compte :\n- Rencontre : #RENCONTRE\n- Date : #DATE\n- Adresse IP : #IP\n\nPour annuler votre inscription, vous pouvez cliquer sur le lien suivant : \n#URL_DEL"), help_text=_("Les mots-clés suivants peuvent être utilisés :<br>#NOM : Nom de l'inscrit<br>#PRENOM : prénom de l'inscrit<br>#ASSO : Association<br>#RENCONTRE : nom complet de la rencontre<br>#DATE : date de la rencontre<br>#NB : nombre d'inscrit<br>#IP : adresse IP de l'inscrit<br>#URL_DEL : adresse de désinscription"))
    champGeo = PointField(verbose_name=_("Lieu"), null=True, )
    zoom = models.FloatField(verbose_name=_("Zoom de la carte"), null=False, default=13, validators=[MinValueValidator(0), MaxValueValidator(19)])
    date = models.DateField(verbose_name=_("Date"),help_text=_('Saisissez donc la date !'), null=False)
    def __unicode__ (self):
        return self.nom

    class Meta:
        verbose_name=_("Recontre ponctuelle")
        verbose_name_plural=_("Recontres ponctuelles")

# -- inscrits

class Inscrits(models.Model):
    unique = models.CharField(verbose_name=_("Numéro unique d'inscription"), max_length=32, unique=True, null=False)
    nom = models.CharField(verbose_name=_("Nom de l'inscrit"), max_length=40, null=False)
    prenom = models.CharField(verbose_name=_("Prénom de l'inscrit"), max_length=40, null=False)
    asso = models.CharField(verbose_name=_("Association"), max_length=150, null=False)
    courriel = models.CharField(verbose_name=_("Adresse de courriel de l'inscrit"), max_length=40, null=False)
    nb = models.IntegerField(verbose_name=_("Nombre d'inscrits"),validators=[pas_plus_de_cinq], default=1)
    rencontre = models.ForeignKey('Rencontre')
    ip = models.CharField(verbose_name=_("Adresse IP"), max_length=40)

    def __unicode__ (self):
        return self.nom

    class Meta:
        verbose_name=_("Inscription")
        verbose_name_plural=_("Inscriptions")


