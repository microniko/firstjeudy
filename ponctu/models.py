# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.db import models

# Create your models here.
from lieux.models import Lieu
from django.core.exceptions import ValidationError

from tinymce import models as tinymce_models

from django.core.validators import MaxValueValidator, MinValueValidator, RegexValidator

from django.utils.translation import gettext_lazy as _

# Description de la base de données
#

def pas_plus_de_cinq(value):
   if value > 5:
      raise ValidationError(_("C'est un peu trop, %s, non ?!") % value)

# --- Rencontre

class Rencontre(models.Model):
    code = models.CharField(verbose_name=_("Raccourci"), help_text=_("Ce code est utilisé dans les URL. Les caractères autorisés sont des lettres, des chiffres et des tirets (-, _)"), validators=[RegexValidator("^[a-zA-Z0-9_-]*$")], max_length =30, unique=True, null=False)
    nom = models.CharField(verbose_name=_("Nom complet"), max_length=30, unique=True, null=False)
    message = tinymce_models.HTMLField(
        verbose_name=_("Message long"), help_text=_("Apparaît sur la page de la Rencontre. Les mots-clés suivants peuvent êtres utilisés :<br>#LIEU_NOM : le nom du lieu<br>#LIEU_URL : l'URL vers la page du lieu<br>#LIEU_ADRESSE : adresse du lieu<br>#HEURE_DEBUT : début de l'évènement<br>#HEURE_FIN : Fin de l'évènement"),
        )
    description = tinymce_models.HTMLField(
        verbose_name=_("Message plus court"), help_text=("Apparaît sur le résumé."),
        )
    pouet = models.TextField(
            verbose_name=_("Message à envoyer sur Mastodon"),
            default=_("À noter dans vos tablettes :\n#DATE à partir de #HEURE_DEBUT, retrouvez-nous à #LIEU_NOM (#LIEU_ADRESSE). Inscriptiion : #URL.\n##LIEU_VILLE ##LIEU_REGION #Rencontre"),
            help_text=_("Les mots-clés peuvent-être utilisés :<br>#LIEU_NOM : le nom du lieu<br>#LIEU_ADRESSE : l'adresse (courte) du lieu<br>#LIEU_VILLE : la ville de l'évènement<br>#LIEU_REGION : la région de l'évènement<br>#URL : l'URL vers l'évènement<br>#DATE : la date<br>#HEURE_DEBUT : l'heure de début<br>#HEURE_FIN : l'heure de fin"),
            max_length=500,
            null=True)
    email = models.TextField(verbose_name=_("Message sur le courriel de confirmation d'inscription"),default=_("Bonjour #PRENOM #NOM.\n\nVotre inscription (pour #NB) a bien été prise en compte :\n- Rencontre : #RENCONTRE\n- Date : #DATE\n- Lieu : #LIEU\n- Adresse IP : #IP\n\nPour annuler votre inscription, vous pouvez cliquer sur le lien suivant : \n#URL_DEL"), help_text=_("Les mots-clés suivants peuvent être utilisés :<br>#NOM : Nom de l'inscrit<br>#PRENOM : prénom de l'inscrit<br>#ASSO : Association<br>#RENCONTRE : nom complet de la rencontre<br>#DATE : date de la rencontre<br>#HEURE_DEBUT : heure du début de la rencontre<br>#HEURE_FIN : heure de fin de la rencontre<br>#LIEU : Nom du lieu de la rencontre<br>#URL_LIEU : Lien vers les détails du lieu de la rencontre<br>#NB : nombre d'inscrit<br>#IP : adresse IP de l'inscrit<br>#URL_ICAL : adresse vers le lien iCalendar<br>#URL_DEL : adresse de désinscription"))
    lieu = models.ForeignKey(Lieu, null=True, on_delete=models.PROTECT)
    tag = models.CharField(verbose_name=_("Tags"), help_text=_("Utile pour la  soumission à l'Agenda du Libre <br><em>Séparés par des espaces, ils ne peuvent contenir que des lettres minuscules, des chiffres et des tirets. Ajoutez le nom de la ou des organisations de l'événement, mais pas de la ville ou de la région.</em>"), max_length=364, null=True)
    date = models.DateField(verbose_name=_("Date"),help_text=_('Saisissez donc la date !'), null=False)
    debut = models.TimeField(verbose_name=_("Heure de début"), null=True, blank=True)
    fin = models.TimeField(verbose_name=_("Heure de fin"), null=True, blank=True)
    limite = models.IntegerField(verbose_name=_("Limite d'inscription"), help_text=_("Nombre d'heure(s) avant le début afin d'alerter le visiteur"), null=True, blank=True)
    autorise_inscription = models.BooleanField(verbose_name=_("Permettre de s'inscrire si la limite est dépassée"), default=True)
    def get_absolute_url(self):
        return "/r/{0}".format(self.code)
    def __str__ (self):
        return self.nom
    class Meta:
        verbose_name=u"Ville (ou lieu) de l'évènement"
        verbose_name_plural=u"Villes (ou lieux) des évènements"


# -- inscrits

class Inscrits(models.Model):
    unique = models.CharField(verbose_name=_("Numéro unique d'inscription"), max_length=32, unique=True, null=False)
    nom = models.CharField(verbose_name=_("Nom de l'inscrit"), max_length=40, null=False)
    prenom = models.CharField(verbose_name=_("Prénom de l'inscrit"), max_length=40, null=False)
    asso = models.CharField(verbose_name=_("Association"), max_length=150, null=False)
    courriel = models.CharField(verbose_name=_("Adresse de courriel de l'inscrit"), max_length=40, null=False)
    nb = models.IntegerField(verbose_name=_("Nombre d'inscrits"),validators=[pas_plus_de_cinq], default=1)
    rencontre = models.ForeignKey('Rencontre', on_delete=models.PROTECT)
    ip = models.CharField(verbose_name=_("Adresse IP"), max_length=40)

    def __str__ (self):
        return self.nom

    class Meta:
        verbose_name=_("Inscription")
        verbose_name_plural=_("Inscriptions")

