#-*- coding: utf-8 -*-

from django.conf.urls import include, url
from django.urls import path
from ponctu.views import InscritsDeletePonctu, inscription, adl, ical

from django.conf import settings
#from django.contrib.staticfiles.urls import staticfiles_urlpatterns
#from django.conf.urls.static import static


#from django.contrib import admin
#from django.contrib.gis import admin as geoadmin
#admin.autodiscover()
app_name = 'ponctu'

urlpatterns = [
    # Examples:
    # url(r'^$', 'firstjeudy.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # Page de désinscription pour les ponctuelles
    path('del/<str:unique>', InscritsDeletePonctu.as_view(), name='inscrit_delete_ponctu'),


    # Fichier ICAL
    path('<str:code>.ics',
        ical,
        name='ical'
        ),
    # Page pour soumettre à l'Agenda du Libre
    path('agenda/<str:code>',
        adl,
        name='adl'
        ),
    # Page d'une recontre : inscription
    path('<str:code>', inscription, name="voir_rencontre"),

]

