#-*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
#from fj.views import InscritsDelete
from ponctu.views import InscritsDeletePonctu

from django.conf import settings
#from django.contrib.staticfiles.urls import staticfiles_urlpatterns
#from django.conf.urls.static import static


#from django.contrib import admin
#from django.contrib.gis import admin as geoadmin
#admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'firstjeudy.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # Page de désinscription pour les ponctuelles
    url(r'^del/?(?P<unique>.+)$', InscritsDeletePonctu.as_view(), name='inscrit_delete_ponctu'),

    # Page d'une recontre : inscription
    url(r'^(?P<code>.+)$', 'ponctu.views.inscription', name="voir_rencontre"),

    #url(r'^(?P<code>.+)/stats$', 'fj.views.stats'),

)

