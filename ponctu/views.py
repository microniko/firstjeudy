# -*- coding: utf-8 -*-
# 
# Copyright (C) 2014  Nicolas Grandjean <nicolas@microniko.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.shortcuts import render

from django.http import Http404
from django.shortcuts import render, get_object_or_404

import uuid
from django import http
from django.views.generic import ListView, DeleteView

from django.core.urlresolvers import reverse_lazy

# dates en français
import locale, time
locale.setlocale(locale.LC_TIME, "fr_FR.utf8")

# Filtrage par date
import datetime

# Envoi de courriels
from django.core.mail import send_mail


# Create your views here.

from models import Rencontre, Inscrits
from fj.forms import InscritsForm   # On importe le formulaire de fj car c'est *exactemet* le même
from django.utils.translation import gettext_lazy as _

from fj.models import Ville

from firstjeudy import  settings

# ------- Page d'inscription     ----------------
def inscription(request, code):
    """Afficher la page d'inscription des rencontres ponctuelles
    - D'abord, sa description
    - Ensuite le formulaire d'inscription
    - Puis le menu
    - Et enfin, la liste des inscrits
    """

    # Pour la description de la ville :
    rencontre = get_object_or_404(Rencontre, code=code, date__gte=datetime.date.today())

    # Le formulaire d'inscription
    if request.method== 'POST':
        form = InscritsForm(request.POST)
        if form.is_valid():
            unique=str(uuid.uuid4())
            adresse_ip =request.META["REMOTE_ADDR"]
            url_fj =request.META["SERVER_NAME"]
            # Est-ce une IPv6 ?
            longueur = len(adresse_ip)
            i=0
            v6=""
            while i < longueur:
                if adresse_ip[i] == ":":
                    v6= u"IPv6 :-)"
                i = i + 1

            # Sauvegarde en base
            Inscrits(
                nom=form.cleaned_data['nom'],
                prenom=form.cleaned_data['prenom'],
                asso=form.cleaned_data['asso'],
                courriel=form.cleaned_data['courriel'],
                nb=form.cleaned_data['nb'],
                unique=unique,
                rencontre_id=rencontre.id,
                ip=adresse_ip
                ).save()
            # Envoi d'un courriel
            # Préparation des variables
            date = str(rencontre.date.strftime('%A %d %B %Y')).decode("utf-8")
            corps = rencontre.email.replace(u"#PRENOM",form.cleaned_data['prenom'])
            corps = corps.replace(u"#NOM",form.cleaned_data['nom'])
            corps = corps.replace(u"#ASSO",form.cleaned_data['asso'])
            corps = corps.replace(u"#NB",str(form.cleaned_data['nb']))
            corps = corps.replace(u"#RENCONTRE",rencontre.nom)
            corps = corps.replace(u"#DATE",date)
            corps = corps.replace(u"#IP",adresse_ip + " " + v6)
            corps = corps.replace(u"#URL_DEL",'http://' + url_fj + '/r/del/'  +unique)
            send_mail(
                rencontre.nom +' du ' + date,
                corps,
                settings.ADMINS[0][0] + " <" + settings.ADMINS[0][1] + ">",
                [form.cleaned_data['courriel']],
                fail_silently=False
                )
            ok = True
    else:
        form=InscritsForm()

    # La liste des recontres
    liste_rencontres = Rencontre.objects.filter(date__gte = datetime.datetime.now())

    # La liste des villes
    liste_villes = Ville.objects.all()

    # La liste des inscrits
    liste_inscrits = Inscrits.objects.filter(rencontre_id=rencontre.id)
    total = 0
    for insc in liste_inscrits:
        total = total + insc.nb


    # Le gabartit
    return render(request, 'rencontre.html', locals())


# ------- Page de suppression     ----------------
class InscritsDeletePonctu(DeleteView):
    model = Inscrits
    context_object_name = "supprime_inscrit"
    template_name = 'del.html'
    success_url = reverse_lazy('index')
    supprime = True
    def get_object(self, queryset=None):
        unique = self.kwargs.get('unique', None)
        return get_object_or_404(Inscrits, unique=unique)




# ------- Page des statistiques     ----------------
